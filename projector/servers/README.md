This is the test server suite. New server personalities can be added here.

Create a new server module in the test/servers directory.

        - create/edit Your-New-Server.py

Add the following minimum to your new Server.py file:
    * Make sure __all__ is properly set with the following
        values:
    __all__ = [ 'Projector', 'BUFFER', 'PORT', 'TIMEOUT_MAX' ]

    * Make sure you have the following variables available:
    BUFFER = <size of input/output buffer>
    PORT = <tcp port to use>
    TIMEOUT_MAX = <float of maximum idle time for connection>

    * Make sure you have the following base class:
    class Projector(telnetlib.Telnet)

In the Projector class, add the following minimum:

    class Projector(telnetlib.Telnet)
        def __init__(<base args>, user=None, password=None)
        def client_connect(self, client, addr)
        def client_disconnect(self, addr)
        def stop(self)
        def run(self)
        def handle_request(self, data)

client_connect(self, client, addr):

    Used for initial authentication.
    client: socket of new connection
    addr: tuple of [ipaddr, port]

    < Code for new connection. If needed, both authenticated
      and non-authenticated connection requests>

    Returns a tuple of
    (Bool, String), where:
        Bool:
            True - Authorized connection
            False - Failed authorization

        String:
            Either a string with initial command or None if no
            initial command requested.

client_disconnect(self, addr):
    Cleanups when client disconnects
    addr: tuple of [ipaddr, port]

stop():
    Cleanups when stop is requested and sets a notification that
    a thread stop has been requested.

run():
    Called when thread starts. Make this a loop with a check
    so cleanups can be done when system is shutdown. Typically
    a while loop is used with a self.running variable check.
    You can name your variable whatever you want, it's just best
    to use a True/False variable so looping can halt when it's set
    to False. Allows for cleanups when system is stopped.

handle_request(data):
    Called whenever data is received
    data = command received from client

Once those minimums are added, fill in the rest of the code to make
your new projector emulator work.

Be sure to add the proper import option in server-net.py file so your
new module will be available.
