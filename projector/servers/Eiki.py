 """example-server.py: Example skeleton module for creating a new server"""

"""
Eiki XL200 Projector Video Inputs:
    INF1='EIKI'
    INF2='XL200'
    INST='11 12 31 32 21 22 13 23 24 25'
        Param   PJLink      Projector
        11      RGB 1       Input 1 - RGB (PC analog)
        12      RGB 2       Input 1 - RGB (Scart)
        13      RGB 3       Input 2 - RGB
        21      VIDEO 1     Input 2 - Video
        22      VIDEO 2     Input 2 - Y, Pb/Cb, Pr/Cr
        23      VIDEO 3     Inout 3 - Video
        24      VIDEO 4     Input 3 - Y, Pb/Cb, Pr/Cr
        25      VIDEO 5     Input 3 - S-video
        31      DIGITAL 1   Input 1 - rgb (PC digital)
        32      DIGITAL 2   Input 1 - RGB (AV HDCP)
        51      NETWORK 1   Input 4 - Network
"""

__version__ = "0.0.2"
_v = __version__.split(".")
__version_hex__ = int(_v[0]) << 24 | \
                  int(_v[1]) << 16 | \
                  int(_v[2]) << 8
__all__ = ['BUFFER', 'PORT', 'SUFFIX', 'TIMEOUT_MAX', 'Projector']

import logging as log
import threading
import time
import random
from time import sleep
from hashlib import md5
from socket import timeout
log.info(f"example-server {__version__} module loading")

log.info("example-server: Setting required variables")
PORT = 4352
BUFFER = 140
TIMEOUT_MAX = 30.0

log.info("example: Setting local constants")
CR = chr(0x0D) # \r
LF = chr(0x0A) # \n
SUFFIX = CR+LF
PREFIX = '%' # Prefix character(s) for emulated server
CLASS = '1'  # Command class if needed
HEADER = PREFIX + CLASS # Modify for your server

log.info("example-server: Creating Projector() class")
class Projector(threading.Thread):
    def __init__(self,
                 group=None,
                 target=None,
                 name='example-server',
                 user=None,
                 password = None
                 args=[],
                 kwargs={}):
        super(Projector, self).__init__(group, target, name, args, kwargs)
        self.name = name
        # Fill in self.info with proper keys for information based on
        # the emulated projector being coded for
        self.info = { }
        self.running = False
        self.user = user
        self.passwd = password
        self.hashes = {} # Dictionary to hold multiple login hashes
        self.hashstring = None

    def stop(self):
        """stop() Clears running flag so thread can safely stop"""
        log.debug('example-server: stop() called')
        self.running = False
        return

    def run(self):
        """run() Runs while thread is alive"""
        log.debug('example-server: run() called')
        self.running = True
        while self.running:
            # Add code to handle runtime settings
            sleep(2)
        return

    def client_connect(self, client, addr):
        """client_connect() Handle connection initialization with client"""
        log.debug(f"example-server: client_connect(addr='{repr(addr)}')")
        # This check should be for user and/or password options
        if self.passwd is None:
            """Add code for non-authenticated connection request"""
            pass
        else:
            """Add code for authenticated connection request"""
            pass
        return

    def client_disconnect(self, addr):
        """client_disconnect() Cleans up from client disconnect"""
        log.debug(f"example-server: client_disconnect(addr='{repr(addr)}')")
        _ip, _port = addr
        return

    def handle_request(self, opts):
        log.debug(f"example-server: handle_request(opts='{opts}')")
        return
