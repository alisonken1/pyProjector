"""PJLink.py: Module to emulate a PJLink server"""

__version__ = "0.1.1"
_v = __version__.split(".")
__version_hex__ = int(_v[0]) << 24 | \
                  int(_v[1]) << 16 | \
                  int(_v[2]) << 8

__all__ = ['BUFFER', 'HEADER', 'PORT', 'SUFFIX', 'TIMEOUT_MAX', 'Projector']

import logging
import threading
import time
import random
from time import sleep
from hashlib import md5
from socket import timeout

from .projectorbase import *

log = logging.getLogger(__name__)

log.info(f"Module loading version {__version__}")

log.info("PJLink: Setting local constants")
PJLINK_PREFIX = '%'
PJLINK_CLASS = '1'

PJLINK_INFO = { 'NAME': "PJLinkProjector", # Can be changed"
                'INF1': "OpenLP",
                'INF2': "PythonProjector",
                'INFO': "Model PS01 Serial P274001"
            }

LAMP_DICT = { "STATUS": 0,
              "NUMBER": 0,
              "LAMP_ON": False,
              "FAN": 0,
              "FAN_ON": False,
              "TEMP": 0,
              "FILTER": 0, # random.randint(0, 6000), # Minutes
              "COVER": 0,
              "TIME": 0, #random.randint(0, 300000), # Minutes
              "CLOCK": None,
              "WARN": False,
            }

log.info("Setting required variables")
BUFFER = 140
HEADER = PJLINK_PREFIX + PJLINK_CLASS
PORT = 4352
SUFFIX = CR
SUFFIX = CR+LF # Telnet needs LF otherwise change back to CR only
TIMEOUT_MAX = 30.0

log.info("Creating PJLinkStatus() class")
class PJLinkStatus(Status):
    """PJLinkStatus(Status)

    Extends basic error code mappings with PJLink code mappings
    """
    # Map some error codes to PJLink physical parts.
    names = { E_NETWORK: "Network",
              E_FAN: "Fan",
              E_LAMP: "Lamp",
              E_TEMP: "Temperature",
              E_COVER: "Cover",
              E_FILTER: "Filter",
              E_PROJECTOR: "Projector"
             }

    # PJLink power status mapping
    power = { S_STANDBY: '0',
              S_ON: '1',
              S_COOLDOWN: '2',
              S_WARMUP: '3',
              '0': S_STANDBY,
              '1': S_ON,
              '2': S_COOLDOWN,
              '3': S_WARMUP
             }
    # PJLink fan/lamp/temp/cover/filter/other mapping
    erst = { E_OK: '0',
             E_WARN: '1',
             E_ERROR: '2',
             '0': E_OK,
             '1': E_WARN,
             '2': E_ERROR
            }

    errors = { E_OK: 'OK',
               E_UNDEFINED: 'ERR1',
               E_PARAMETER: 'ERR2',
               E_UNAVAILABLE: 'ERR3',
               E_PROJECTOR: 'ERR4',
               E_AUTHENTICATION: 'ERRA',
               'OK': E_OK,
               'ERR1': E_UNDEFINED,
               'ERR2': E_PARAMETER,
               'ERR3': E_UNAVAILABLE,
               'ERR4': E_PROJECTOR,
               'ERRA': E_AUTHENTICATION
              }

    # Define inputs - This lists all valid PJLink inputs
    inputs = { '11': "RGB 1",
               '12': "RGB 2",
               '13': "RGB 3",
               '21': "VIDEO 1",
               '22': "VIDEO 2",
               '23': "VIDEO 3",
               '31': "DIGITAL 1",
               '32': "DIGITAL 2",
               '33': "DIGITAL 3",
               '41': "STORAGE 1",
               '42': "STORAGE 2",
               '51': "NETWORK 1",
               '52': "NETWORK 2"
             }

log.debug("Setting non-standard help")
_CMDHELP = f"""

All command require a prefix.
    PREFIX = {PJLINK_PREFIX+PJLINK_CLASS}

Example command to request power status:
    {PJLINK_PREFIX+PJLINK_CLASS}POWR ?<return>

PJLink options are:
    POWR [? 0 1]
    INPT [? {PJLinkStatus.inputs.keys()}]
    AVMT [? 10 11 20 21 30 31]
    LAMP ?
    INST ?
    NAME ?
    INF1 ?
    INF2 ?
    INFO ?
    CLSS ?

    Non-error returns will be:
        OK - Command executed
        <data> - Some string based on the command given

    Error returns will be:
        ERR1 - Invalid command
        ERR2 - Invalid option
        ERR3 - Unavailable time
        ERR4 - Projector/Display error
        ERRA - Authentication error

"""

log.info("Creating Projector() class")
class Projector(ProjectorBase):
    def __init__(self,
                 group=None, # Thread required
                 target=None, # Thread required
                 name='PJLink',
                 user=None,
                 password=None,
                 failcheck=None,
                 lamps=None,
                 args=[], # Thread required
                 kwargs={}): # Thread required
        _pwd = password if password is None else 'password'
        log.info(f"Projector.__init__(user='{user}' password='{_pwd}')")
        super(Projector, self).__init__(group, target, name, args, kwargs)
        self.client_ip = None
        self.name = name
        self.running = False
        self.user = user
        if password == 'TESTME':
            log.debug('Projector.__init__(): Setting self.passwd to JBMIAProjectorLink')
            self.passwd = 'JBMIAProjectorLink'
        else:
            log.debug('Projector.__init__(): Using password option')
            self.passwd = password
        self.hashes = {} # Dictionary to hold multiple login hashes
        self.shutter = False # True=shutter close=False shutter open
        self.audio = False # True=audio muted False=audio normal
        self.source = '11' # Default to first analog input
        self.PROJ_ERR = None
        self.PROJ_PART = [] # May have multiple failures
        self.PROJ_ERRSTAT = { 1: 0,  # Fan
                              2: 0,  # Lamp
                              3: 0,  # Temperature
                              4: 0,  # Cover open
                              5: 0,  # Filter
                              6: 0   # Other
                            }
        # Time between random failure check (minutes)
        if failcheck is None or failcheck.isdigit():
            self.failcheck = failcheck
        else:
            self.failcheck = None
            log.warning("Projector.__init__(): Invalid failcheck option - disabling failcheck")
        self.failclock = None   # Clock for next random failures
        self.timer = None
        """ self.powerstat
                S_STANDBY: 0
                S_ON: 1
                S_COOLDOWN: 2
                S_WARMUP: 3
        """
        self.powerstat = S_STANDBY # Initial condition is standby
        """ self.lamp

        Dictionary of lamps in projector.
        Each lamp is another dictionary of status
            STATUS: See PJLinkStatus.[erst | power]
            FAN:    Fan state S_OFF, S_ON, E_ERR
            TIME:   Time in minutes lamp has been in the ON state.
                    Initial lamp time is a random int from 0 (new)
                    to 300000 minutes (approx. 5000 hours)
            CLOCK:  time.time() when lamp was initially turned ON.
                    This includes warmup/cooldown times.
                    None when lamp is off.
            FILTER: Time since last filter cleaning
            COVER:  Whether the lamp access cover is open
            WARN:   Warning indicator for lamp time or filter time
        """
        # First define number of lamps, then fill out in next step

        self.lamp = {}
        if lamps is None:
            _lamps = 1
        elif (type(lamps) is type(str)) and not lamps.isdigit():
            _lamps = 1
            log.warning("Projector.__init__(): Invalid option for lamps - defaulting to 1")
        else:
            _lamps = int(lamps)
        for i in range(1,_lamps+1): # Taken from initialization
            self.lamp[i] = None
        for i in self.lamp:
            self.lamp[i] = LAMP_DICT.copy()
            self.lamp[i]['FILTER'] = random.randint(0,6000)
            self.lamp[i]['TIME'] = random.randint(0,300000)
            self.lamp[i]['LAMP_ON'] = False
            if self.lamp[i]['TIME'] >= 240000 or self.lamp[i]['FILTER'] > 6000:
                # Lamp has > 4000 hours runtime
                # Filter has > 100 hours since last cleaning
                self.lamp[i]['WARN'] = True
            log.debug(f"Projector.__init__(): Lamp {i} TIME={self.lamp[i]['TIME']}")
            log.debug(f"Projector.__init__(): Lamp {i} FILTER={self.lamp[i]['FILTER']}")
            log.debug(f"Projector.__init__(): Lamp {i} WARN={self.lamp[i]['WARN']}")

    ######################################################################
    #                        Required functions                          #
    ######################################################################
    def client_disconnect(self, addr):
        """client_disconnect(addr)

        Removes connection cached hash key.
        """
        log.info(f"Projector.client_disconnect(addr={repr(addr)}")
        _ip, _port = addr
        _key = f"{_ip}:{_port}"
        if self.hashes.has_key(_key):
            self.hashes.pop(_key)
        return

    def client_connect(self, client, addr):
        """client_connect(client, addr)

        Handle connection initialization with client and
        adds new client to self.hashes.
        """
        log.info(f"Projector.client_connect(addr='{repr(addr)}')")
        self.client_ip, self.client_port = addr
        if self.passwd is None:
            log.debug(f"Projector.client_connect({self.client_ip}) Unauthenticated connection")
            client.send(bytes(f"PJLINK 0{SUFFIX}", encoding='utf-8'))
            return (True, 'OK')

        else:
            # For testing purposes:
            # _rand = '498e4a67'
            # Password is 'JBMIAProjectorLink'
            # Return hash should be '5d8409bc1c3fa39749434aa3a5c38682'
            if self.passwd == 'JBMIAProjectorLink':
                log.debug(f"Projector({self.client_ip}): self.passwd == 'JBMIAProjectorLink'")
                log.debug(f"Projector({self.client_ip}): setting _rand='498e4a67'")
                _rand = '498e4a67'
            else:
                _rand = hex(random.randint(268435456, 4294967295))[2:]
                log.debug(f"Projector({self.client_ip}): Setting _rand to '{_rand}'")
            _hashed = md5()
            _hashed.update(_rand)
            _hashed.update(self.passwd)
            _digest = str(_hashed.hexdigest())[:32]
            _ip, _port = addr
            _key = f"{_ip}:{_port}"
            self.hashes[_key] = _digest
            try:
                log.debug(f"Projector({self.client_ip}).client_connect(): Sending 'PJLINK 1 randomkey'")
                client.send(f"PJLINK 1 {_rand}{SUFFIX}")
                data = client.recv(BUFFER).decode().strip()
                log.debug(f"Projector.server_connect({self.client_ip}) Received {data}")
            except (timeout, e):
                log.debug(f"Projector.server_connect({self.client_ip}): Timeout")
                return (False, None)

            if len(data) > 32 and data[32:34] == HEADER:
                log.debug(f"Projector.server_connect({self.client_ip}): Authentication with command")
                _a = data[:32]
                _d = data[32:].strip()
            else:
                log.debug(f"Projector.server_connect({self.client_ip}): Authentication only")
                _a = data[:32]
                _d = None

            log.debug(f"Projector.server_connect({self.client_ip}): {_a} Received")
            log.debug(f"Projector.server_connect({self.client_ip}): {self.hashes[_key]} Expected")
            if _a == self.hashes[_key]:
                log.debug(f"Projector.server_connect({self.client_ip}): Validated login data='{_d}'")
                return (True, _d)
            else:
                log.debug(f"Projector.server_connect({self.client_ip}): Invalid login")
                client.send(f"PJLINK={PJLinkStatus.errors[E_AUTHENTICATION]}{SUFFIX}")
                return (False, None)

        return (False, None) # Catchall failed

    def stop(self):
        """stop()

        Clears running flag so thread can safely stop
        """
        log.info('Projector.stop() called')
        self.running = False
        return

    def run(self):
        """run()

        Called when thread is started, then checks status of projector
        for any changes.
        """
        log.info('Projector.run() called')
        self.running = True
        self._power_change(S_STANDBY)
        while self.running:
            sleep(2.0) # 2 second intervals between checks
            if self.timer is None: continue
            if time.time() < self.timer: continue
            # Time to do something
            # The only changes should be warmup -> on/cooldown -> standby
            log.debug("Projector.run() Time to change something")
            if self.powerstat == S_WARMUP:
                self._power_change(S_ON)
            elif self.powerstat == S_COOLDOWN:
                self._power_change(S_STANDBY)

            if self.failclock is not None and self.failclock < time.time():
                _random_fail()

            continue # Not required, but explicit > implicit
        return

    def handle_request(self, data):
        """handle_request(data)

        Called from main thread to handle projector requests.
        """
        log.info(f"Projector({self.client_ip}).handle_request(data='{data}')")
        if data is None:
            return
        if data.upper() == '--HELPME--':
            return _CMDHELP
        if len(data) < 2:
            log.debug(f"Projector({self.client_ip}).handle_request(): Packet length < 2")
            return ''
        elif data[0] != PJLINK_PREFIX:
            log.debug(f"Projector({self.client_ip}).handle_request(): data[0] != {PJLINK_PREFIX}")
            return ''
        elif data[1] != PJLINK_CLASS:
            log.debug(f"Projector({self.client_ip}).handle_request(): Invalid class {data[1]} != {PJLINK_CLASS}")
            return ''

        _r = None
        if len(data) < 8:
            log.debug(f"Projector({self.client_ip}).handle_request(): Packet length < 8")
            log.debug(f"Projector({self.client_ip}).handle_request(): Packet length: {len(data)}")
            # Minimum packet length not met
            return f"{data}={PJLinkStatus.errors[E_UNDEFINED]}"
        elif data[6] != ' ':
            log.debug(F"Projector({self.client_ip}).handle_request(): char 7 not a space: {data[6]}")
            return f"{data}={PJLinkStatus.errors[E_UNDEFINED]}"
        else:
            _hdr = data[:2]
            _cmd, _param = data[2:].split(' ')

        if _r is not None:
            pass # Already have a reply
        else:
            # Commands with mutliple options
            if _cmd.upper() == 'POWR':
                _d = self.power(_param)
                if _d == S_OK:
                    _r = PJLinkStatus.errors[S_OK]
                else:
                    _r = _d
            elif _cmd.upper() == 'INPT':
                _d = self.input_select(_param)
                if _d == S_OK:
                    _r = PJLinkStatus.errors[S_OK]
                else:
                    _r = _d
            elif _cmd.upper() == 'AVMT':
                _d = self.avmute(_param)
                if _d == S_OK:
                    _r = PJLinkStatus.errors[S_OK]
                else:
                    _r = _d

            # The rest require ? as parameter and nothing else
            elif _param != '?':
                _r = PJLinkStatus[E_PARAMETER]
            elif _cmd.upper() == 'ERST':
                _r = self.error_status(_param)
            elif _cmd.upper() == 'LAMP':
                _r = self.lamp_status(_param)
            elif _cmd.upper() == 'INST':
                _r = self.input_list(_param)
            elif _cmd.upper() == 'NAME':
                _r = PJLINK_INFO['NAME']
            elif _cmd.upper() == 'INF1':
                _r = PJLINK_INFO['INF1']
            elif _cmd.upper() == 'INF2':
                _r = PJLINK_INFO['INF2']
            elif _cmd.upper() == 'INFO':
                _r = PJLINK_INFO['INFO']
            elif _cmd.upper() == 'CLSS':
                _r = PJLINK_CLASS
            else:
                # Unknown command
                _r = PJLinkStatus.errors[E_UNDEFINED]
        _d = f"{_hdr}{_cmd}={_r}"
        log.debug(f"Projector({self.client_ip}).handle_request() returning '{_d}')")
        return _d

    ######################################################################
    #                        Local functions                             #
    ######################################################################
    def _random_fail(self):
        """_random_fail()

        Random check if a part failed and sets self.PROJ_ERRSTAT[]
        list to which part(s) are failed.
        """
        log.info(f"Projector({self.client_ip})._random_fail() called")
        if int(random.random()) == 1:
            _list = [ 'POWER', 'LENS' ]
            for i in self.lamp:
                _list.append(f"LAMP{self.lamp[i]['NUMBER']}")
                _list.append(f"FAN{self.lamp[i]['NUMBER']}")
                _list.append(f"COVER{self.lamp[i]['NUMBER']}")
                _list.append(f"FILTER{self.lamp[i]['NUMBER']}")
                if self.lamp[i]['TIME'] > 30000:
                    # Lamp exceeds rated time - extra chance of failure
                    _list.append(f"LAMP{self.lamp[i]['NUMBER']}")
            failed = random.choice(_list)
            self.PROJ_PART = failed
            if 'FAN' in failed:
                if failed not in self.PROJ_PART:
                    self.PROJ_ERRSTAT[1] = 2
            elif 'LAMP' in failed:
                if failed not in self.PROJ_PART:
                    self.PROJ_ERRSTAT[2] = 2
            elif 'TEMP' in failed:
                # Probably fan failed or filter clogged
                if failed not in self.PROJ_PART:
                    self.PROJ_ERRSTAT[3] = 2
            elif 'COVER' in failed:
                # Cover open
                if failed not in self.PROJ_PART:
                    self.PROJ_ERRSTAT[4] = 2
            elif 'FILTER' in failed:
                # Filter clogged
                if failed not in self.PROJ_PART:
                    self.PROJ_ERRSTAT[5] = 2
            else:
                self.PROJ_ERRSTAT[6] = 2

    def _lamp_clock_update(self, lamp, stop=False):
        """
        Update the clock time for the lamp
        """
        if lamp['CLOCK'] is not None:
            _t = int(time.time() - lamp['CLOCK'])
            lamp['TIME'] = lamp['TIME'] + (_t / 60)
        if stop:
            lamp['CLOCK'] = None

    def _lamp_change(self, lamp, opt, failed='LAMP'):
        """_lamp_change(lamp, opt)

        Handle lamp changes. Must be called with a dict of a single lamp.
        """
        log.info(f"Projector({self.client_ip})._lamp_change(opt='{Status.keys[opt]}')")

        if not type(lamp) == type({}):
            log.warning(f"Projector({self.client_ip})._lamp_change() lamp not a dict")
            log.warning(f"Projector({self.client_ip})._lamp_change(lamp): {lamp}")
            return E_PROJECTOR
        elif opt is None:
            # Update clock below
            pass
        elif opt != S_OFF and \
                 opt not in PJLinkStatus.power and \
                 opt not in PJLinkStatus.erst:
            log.warning(f"Projector({self.client_ip})._lamp_change() invalid option {opt}")
            return E_PARAMETER

        _r = S_OK
        if opt is None:
            self._lamp_clock_update(lamp)
        elif opt == E_ERROR:
            _r = PJLinkStatus.errors[E_PROJECTOR]
            if failed == 'LAMP':
                self._lamp_clock_update(lamp, True)
                lamp['STATUS'] = PJLinkStatus.erst[E_ERROR]
                PROJ_ERRSTAT[0] = PJLinkStatus.erst[E_ERROR]
            elif failed == 'FAN':
                lamp['FAN'] = PJLinkStatus.erst[E_ERROR]
                lamp['FAN_ON'] = False
                lamp['TEMP'] = PJLinkStatus.erst[E_WARN]
            elif failed == 'COVER':
                lamp['COVER'] = PJLinkStatus.erst[E_ERROR]
                lamp['TEMP'] = PJLinkStatus.erst[E_WARN]
        elif opt == S_ON:
            lamp['FAN_ON'] = True
            lamp['LAMP_ON'] = True
        elif opt == S_COOLDOWN:
            self._lamp_clock_update(lamp, True)
            lamp['STATUS'] = S_OFF
            lamp['LAMP_ON'] = False
        elif opt == S_WARMUP:
            lamp['STATUS'] = S_ON
            lamp['FAN_ON'] = True
            lamp['CLOCK'] = time.time()
            lamp['LAMP_ON'] = True
        return _r

    def _power_change(self, opt):
        """_power_change(opt)

        Handle power setting changes.
        """
        log.info(f"Projector({self.client_ip})._power_change(opt='{Status.keys[opt]}')")
        log.info(f"Projector({self.client_ip})._power_change() self.timer={self.timer}")
        if self.PROJ_ERR is not None:
            return self.PROJ_ERR
        elif self.timer is not None and time.time() < self.timer:
            return PJLinkStatus.errors[E_UNAVAILABLE]
        elif not opt in PJLinkStatus.power:
            return PJLinkStatus.errors[E_PARAMETER]

        # Error checks passed - time to change something
        lampset = None
        _r = S_OK
        self.powerstat = opt
        if opt == S_WARMUP:
            self.timer = time.time() + 20.0
            lampset = S_ON
        elif opt == S_STANDBY:
            self.timer = None
            lampset = S_OFF
        elif self.powerstat == S_COOLDOWN:
            self.timer = time.time() + 20.0
            lampset = S_COOLDOWN
        elif opt == S_ON:
            self.timer = None
            # No lamp change

        _dd = S_OK
        if lampset is not None:
            for i in self.lamp:
                _d = self._lamp_change(lamp=self.lamp[i], opt=lampset)
                if _d != S_OK:
                    _dd = _d
                log.debug(f"PJLink({self.client_ip})._power_change(lamp={i}) received lamp status {_d}")

        if _dd != S_OK or  _r != S_OK:
            _r = PJLinkStatus.errors[E_PROJECTOR]

        log.debug(f"Projector({self.client_ip})._power_change() returning {PJLinkStatus.keys[_r]}")
        return _r

    def power(self, opt):
        """power(opt)

        Handle POWR command requests.
        """
        log.info(f"Projector({self.client_ip}).power(opt='{opt}')")
        log.debug(f"Projector({self.client_ip}).powerstat = {PJLinkStatus.keys[self.powerstat]}")
        if opt not in '?01':
            log.debug(f"Projector({self.client_ip}).power(): opt {opt} not in '?01'")
            return PJLinkStatus.errors[E_PARAMETER]
        elif self.PROJ_ERR is not None:
            log.debug(f"Projector({self.client_ip}).power() returning self.PROJ_ERR")
            return self.PROJ_ERR

        log.debug(f"Projector({self.client_ip}).power(): Checking opt {opt}")
        if opt == '?':
            if self.powerstat in PJLinkStatus.power:
                return PJLinkStatus.power[self.powerstat]
            else:
                return PJLinkStatus.errors[E_PROJECTOR]

        log.debug(f"Projector({self.client_ip}).power(): Checking for unavailable time")
        if self.powerstat == S_WARMUP or self.powerstat ==  S_COOLDOWN:
            return PJLinkStatus.errors[E_UNAVAILABLE]

        log.debug(f"Projector({self.client_ip}).power(): Checking for set on/off")
        _r = S_OK
        if opt == '1':
            if self.powerstat == S_ON:
                return S_OK
            else:
                _d = self._power_change(S_WARMUP)
        elif opt == '0':
            if self.powerstat == S_STANDBY:
                return S_OK
            else:
                _d = self._power_change(S_COOLDOWN)

        if _d != S_OK:
            return _d
        else:
            return _r

    def error_status(self, opt):
        """error_status(opt)

        Returns the current status of:
            fan[s],
            lamp[s],
            temperature,
            cover open,
            filter[s],
            other
        """
        log.info(f"Projector({self.client_ip}).error_status(opt={opt})")
        if opt != "?":
            return PJLinkStatus.errors[E_PARAMETER]
        elif self.PROJ_ERR is not None:
            return self.PROJ_ERR

        return "%s%s%s%s%s%s" % (self.PROJ_ERRSTAT[1],
                                 self.PROJ_ERRSTAT[2],
                                 self.PROJ_ERRSTAT[3],
                                 self.PROJ_ERRSTAT[4],
                                 self.PROJ_ERRSTAT[5],
                                 self.PROJ_ERRSTAT[6]
                                 )

    def avmute(self, opt):
        """avmute(opt)

        Handles AVMT requests.
        """
        log.info(f"Projector({self.client_ip}).avmute(opt={opt})")
        if self.PROJ_ERR is not None: return self.PROJ_ERR
        if opt == '?':
            if self.powerstat == 2:
                return PJLinkStatus.errors[3]
            elif not self.audio and not self.shutter:
                return '30'
            elif self.audio and self.shutter:
                return '31'
            elif self.shutter:
                # video mute (shutter closed)
                return '11'
            else:
                # Audio muted only
                return '21'
        elif opt == '11':
            self.shutter = True
        elif opt == '10':
            self.shutter = False
        elif opt == '21':
            self.audio = True
        elif opt == '20':
            self.audio = False
        elif opt == '31':
            self.shutter = True
            self.audio = True
        elif opt == '30':
            self.shutter = False
            self.audio = False
        else:
            return PJLinkStatus.errors[E_PARAMETER]
        return PJLinkStatus.errors[S_OK]

    def lamp_status(self, opt):
        """lamp_status(opt)

        Returns the lamp hours and current on/off status of
        available lamps.
        """
        log.info(f"Projector({self.client_ip}).lamp_status(): called")
        if self.PROJ_ERR is not None:
            return self.PROJ_ERR
        if opt != '?': return PJLinkStatus.errors[E_PARAMETER]
        _r = ''
        for i in self.lamp:
            _l = "1" if self.lamp[i]['LAMP_ON'] else "0"
            _r = f"{_r} {self.lamp[i]['TIME']} {_l}"
        return _r.strip()

    def input_list(self, opt):
        """"input_list(opt)

        Handles INST request.
        Returns a string of the available source inputs.
        """
        log.info(f"Projector({self.client_ip}).input_list(opt='{opt}')")
        if self.PROJ_ERR is not None:
            return self.PROJ_ERR
        if opt != '?':
            log.debug(f"Projector({self.client_ip}).input_list(opt='{opt}')")
            return PJLinkStatus.errors[E_PARAMETER]
        if self.powerstat == S_STANDBY:
            log.debug(f"Projector({self.client_ip}).input_list() self.powerstat={PJLinkStatus.codes[self.powerstat]}")
            return PJLinkStatus.errors[E_UNAVAILABLE]
        _r = []
        for key in PJLinkStatus.inputs.keys():
            _r.append(key)
        log.debug(f"Projector({self.client_ip}).input_list(): _r")
        _r.sort()
        return ' '.join(_r).strip()

    def input_select(self, opt):
        """input_select(opt)

        Handles INPT requests.
        """
        log.info(f"Projector({self.client_ip}).input_select(opt='opt')")
        if self.PROJ_ERR is not None:
            return self.PROJ_ERR
        if opt == '?':
            return self.source
        elif self.powerstat != S_ON and self.powerstat != S_WARMUP:
            _ps = PJLinkStatus.codes[self.powerstat]
            _son = PJLinkStatus.codes[S_ON]
            log.debug(f"Projector({self.client_ip}).input_select(opt={opt}) Power state {_ps} != {_son}")
            return PJLinkStatus.errors[E_UNAVAILABLE]
        elif opt not in PJLinkStatus.inputs:
            log.debug(f"Projector({self.client_ip}).input_select(opt={opt}) Invalid input")
            return PJLinkStatus.errors[E_PARAMETER]
        self.source = opt
        return PJLinkStatus.errors[S_OK]
