# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=80 tabstop=4 softtabstop=4

"""projectorbase.py: Base module for projector emulators"""

__version__ = "0.0.2"
_v = __version__.split(".")
__version_hex__ = int(_v[0]) << 24 | \
                  int(_v[1]) << 16 | \
                  int(_v[2]) << 8

# Define exportables for *
__all__ = ['CR', 'LF', 'BUFFER', 'HEADER', 'PORT', 'SUFFIX', 'TIMEOUT_MAX',
           'S_OK', 'E_OK', 'E_GENERAL', 'E_NOTCONNECTED', 'E_NETWORK', 'E_FAN',
           'E_LAMP', 'E_TEMP', 'E_COVER', 'E_FILTER', 'E_AUTHENTICATION',
           'E_UNDEFINED', 'E_PARAMETER', 'E_UNAVAILABLE', 'E_PROJECTOR',
           'E_INVALID_DATA', 'E_WARN', 'E_ERROR', 'S_STANDBY', 'S_ON',
           'S_WARMUP', 'S_COOLDOWN', 'S_NOT_CONNECTED', 'S_CONNECTING',
           'S_STATUS', 'S_OFF', 'S_INITIALIZE', 'S_INFO', 'Status',
           'ProjectorError', 'ProjectorNotConnectedError',
           'ProjectorLampError', 'ProjectorFailedError',
           'ProjectorNetworkError', 'ProjectorBase']

import os
import errno
import logging
import socket
import sys
import threading
import time

# Log should already be setup before getting here
log = logging.getLogger(__name__)

# Common codes
log.debug('projectorbase: Setting constants')
CR = chr(0x0D) # *nix \r
LF = chr(0x0A) # *nix \n
BUFFER = 140 # Default PJLink + 4
HEADER = '%1' # Default PJLink class 1 header
PORT = 4352 # Default PJLink
SUFFIX = CR # Default PJLink
TIMEOUT_MAX = 30.0 # Default PJLink

# Enumeration of status and error codes used
S_OK = 0
E_OK = 0
# Error codes. Start at 200 so we don't duplicate system error codes.
E_GENERAL = 200 # Unknown error
E_NOTCONNECTED = 201
E_NETWORK = 202
E_FAN = 203
E_LAMP = 204
E_TEMP = 205
E_COVER = 206
E_FILTER = 207
E_AUTHENTICATION = 208 # PJLink ERRA
E_UNDEFINED = 209      # PJLink ERR1
E_PARAMETER = 210      # PJLink ERR2
E_UNAVAILABLE = 211    # PJLink ERR3
E_PROJECTOR = 212      # PJLink ERR4
E_INVALID_DATA = 213
E_WARN = 214
E_ERROR = 215

# Status codes. Start with 300 so we don't duplicate system error codes.
S_STANDBY = 300        # PJLink power 0
S_ON = 301             # PJLink power 1
S_WARMUP = 302         # PJlink power 2
S_COOLDOWN = 303       # PJLink power 3
S_NOT_CONNECTED = 304
S_CONNECTING = 305
S_STATUS = 306
S_OFF = 307
S_INITIALIZE = 308
S_INFO = 309

# Enumeration classes
class Status(object):

    # Start at 200 so we don't cover system error codes
    keys = { S_OK: 'S_OK',
             E_GENERAL: 'E_GENERAL',
             E_NOTCONNECTED: 'E_NOTCONNECTED',
             E_NETWORK: 'E_NETWORK',
             E_FAN: 'E_FAN',
             E_LAMP: 'E_LAMP',
             E_TEMP: 'E_TEMP',
             E_COVER: 'E_COVER',
             E_FILTER: 'E_FILTER',
             E_AUTHENTICATION: 'E_AUTHENTICATION',
             E_UNDEFINED: 'E_UNDEFINED',
             E_PARAMETER: 'E_PARAMETER',
             E_UNAVAILABLE: 'E_UNAVAILABLE',
             E_PROJECTOR: 'E_PROJECTOR',
             E_INVALID_DATA: 'E_INVALID_DATA',
             E_WARN: 'E_WARN',
             E_ERROR: 'E_ERROR',
             S_STANDBY: 'S_STANDBY',
             S_ON: 'S_ON',
             S_WARMUP: 'S_WARMUP',
             S_COOLDOWN: 'S_COOLDOWN',
             S_NOT_CONNECTED: 'S_NOT_CONNECTED',
             S_CONNECTING: 'S_CONNECTING',
             S_STATUS: 'S_STATUS',
             S_OFF: 'S_OFF',
             S_INITIALIZE: 'S_INITIALIZE',
             S_INFO: 'S_INFO'
             }

    codes = { S_OK: 'OK',
              E_OK: 'OK',
              E_GENERAL: "General projector error",
              E_NOTCONNECTED: "Not connected error",
              E_NETWORK: "Network error",
              E_LAMP: "Lamp error",
              E_FAN: "Fan error",
              E_TEMP: "High temperature detected",
              E_COVER: "Cover open detected",
              E_FILTER: "Check filter",
              E_AUTHENTICATION: "Authentication Error",
              E_UNDEFINED: "Undefined Command",
              E_PARAMETER: "Invalid Parameter",
              E_UNAVAILABLE: "Projector Busy",
              E_PROJECTOR: "Projector/Display Error",
              E_INVALID_DATA: "Invald packet received",
              E_WARN: "Warning condition detected",
              E_ERROR: "Error condition detected",
              S_NOT_CONNECTED: "Not connected",
              S_CONNECTING: "Connecting",
              S_STATUS: "Getting status",
              S_OFF: "Off",
              S_INITIALIZE: "Initialize in progress",
              S_STANDBY: "Power standby",
              S_WARMUP: "Warmup in progress",
              S_ON: "Power on",
              S_COOLDOWN: "Cooldown in progress",
              S_INFO: "Projector Information availble"
              }

# Error classes
class ProjectorError(Exception):
    """
    Base projector error class
    """
    def __init__(self, errno=None, message=None):
        self.errno = errno if errno is not None else E_GENERAL
        self.message = message if message is not None else 'General projector error'
    def __repr__(self):
        return ((self.errno, self.message))
    def __str__(self):
        return self.message

class ProjectorNotConnectedError(ProjectorError):
    """
    Projector not connected
    """
    def __init__(self, errno=E_NOTCONNECTED,
                       message=u'Projector Not Connected'):
        super(ProjectorNotConnectedError, self).__init__(errno, message)

class ProjectorLampError(ProjectorError):
    """
    Lamp failure
    """
    def __init__(self, errno=E_LAMP,
                       message=u'Lamp Failure'):
        super(ProjectorLampError, self).__init__(errno, message)

class ProjectorFailedError(ProjectorError):
    """
    General projector failure
    """
    def __init__(self, errno=E_GENERAL,
                       message=u'Unknown projector failure'):
        super(ProjectorFailedError, self).__init__(errno, message)

class ProjectorNetworkError(ProjectorError):
    """
    Network failure
    """
    def __init__(self, errno=E_NETWORK,
                       message=u'Projector network error'):
        super(ProjectorNetworkError, self).__init__(errno, message)

class ProjectorFanError(ProjectorError):
    """
    Fan failure
    """
    def __init__(self, errno=E_FAN,
                       message=u'Projector fan failure'):
        super(ProjectorFanError, self).__init__(errno, message)

log.info('Creating base projector class')
class ProjectorBase(threading.Thread):
    def __init__(self,
                group=None, # Thread required - defined as None at this time
                target=None, # Thread required
                name='ProjectorBase',
                user=None,
                password=None,
                host=None,
                port=PORT,
                args=[],
                kwargs={}
                ):
        super(ProjectorBase, self).__init__(group, target, name, args, kwargs)
        self.class_ = '1' # Current PJLink class defined
        self.connected = False
        self.connecting = False
        self.host = host
        self.name = name
        self.password = password
        self.port = port
        self.prefix = '%' # PJlink command prefix
        self.running = False # Keep track of thread running status
        self.status = 0
        self.status_error = None
        self.status_message = ''
        self.status_time = time.time()
        self.suffix = CR # Default PJLink suffix
        self.user = user
        self.header = self.prefix + self.class_

    # Required methods to override
    def run(self):
        """ProjectorBase.run()

        Method used to initialize and run thread.
        """
        self.__run = False
        assert self.__run, "You must override this method."

    def client_connect(self):
        """ProjectorBase.sock_connect()

        Method used to connect to projector.
        You must define steps for authenticated and unauthenticated connections.
        """
        self.__connect = False
        assert self.__connect, "You must override this method."

    # Methods that should not need to be overridden

    def _set_status(self, code=None, message=None):
        if self.status_error is not None:
            log.debug(f"{self.name}({self.host}): _set_status(self.status_error)")
            _code = self.status_error.errno
            _msg = self.status_error.message
        elif code == self.status:
            # No status change
            return
        else:
            log.debug(f"{self.name}({self.host}): _set_status(code={code}, message='{message}')")
            _code = code
            if code in Status.codes:
                _msg = Status.codes[code]
            else:
                _msg = message

        self.status = _code
        self.status_message = _msg

        """
        If a callback function is registered, then send status update.
        """
        if self.callback is None:
            return

        if self.callback_code == self.status and time.time() < self.status_time:
            return

        self.callback((self.status, self.status_message))
        self.callback_code = self.status
        self.status_time = time.time() # Last time callback was called
        return

    def send(self, data, timeout=5.0):
        """ProjectorBase.send()

        Send a packet of data. Assumes telnet protocol.
        """
        log.debug(f"{self.name}({self.host}).send() called")
        if self.status_error is not None:
            log.debug(f"{self.name}({self.host}): Error status code {self.status_error.errno}")
            return

        if self.socket is None and not self.connecting:
            log.debug(f"{self.name}({self.host}).send(): Reconnecting")
            self.sock_connect()

        try:
            log.debug(f"{self.name}({self.host}).send(data='{data}')")
            self.socket.write(f"{self.header}{data}{self.SUFFIX}")
        except socket.error as e:
            try:
                _errno = e.errno
            except AttributeError:
                _errno = None

            log.debug(f"{self.name}({self.host}).send(): Socket error ({e.errno}) {e.message}")
            self.status_error = ProjectorNetworkError(errno=_errno,
                                                      message=e.message)

    def recv(self, search, timeout=5.0):
        """ProjectorBase.recv()

        Receive data. Assumes telnet protocol.
        """
        log.debug(f"{self.name}({self.host}).recv(): Fetching data")
        if self.status_error is not None:
            return None

        if self.socket is None:
            log.debug(f"ProjectorBase.recv({self.host}): No socket open")
            self.status_error = ProjectorNotConnectedError(
                errno=E_NOTCONNECTED,
                message=u'Socket not open')
            self._set_status()

        try:
            for i in range(5):
                # Clean out empty lines
                data = self.socket.read_until(search, timeout)
                if len(data) < 3:
                    continue
                else:
                    break
            if data is None:
                self.socket.close()
                self.socket=None
                self.status_error = ProjectorNetworkError(errno=E_NOTCONNECTED,
                    message=f"{self.name}({self.host}): Socket closed by other side")
        except socket.timeout as e:
            log.debug(f"{self.name}({self.host})_recv(): Timeout error: {e.message}")
            self.socket.close()
            self.socket = None
            self.status_error = ProjectorNetworkError(errno=e.errno,
                message=f"{self.name}({self.host}): Timeout error: e.message")
        except socket.error as e:
            log.debug(f"{self.name}({self.host})_recv(): Socket error: {e.message}")
            self.socket.close()
            self.socket = None
            self.status_error = ProjectorNetworkError(errno=e.errno,
                message=f"{self.name}({self.host}): Socket error: {e.message}")
        except EOFError as e:
            log.debug(f"{self.name}({self.host})_recv(): Socket closed by other side: {e.message}")
            self.socket.close()
            self.socket=None
            self.status_error = ProjectorNetworkError(errno=E_NOTCONNECTED,
                message=f"{self.name}(self.host): Socket closed by other side")

        if self.status_error is not None:
            self._set_status()
            return None
        else:
            log.debug(f"{self.name}({self.host}).recv() returning '{data.strip()}'")
            return data.strip()
