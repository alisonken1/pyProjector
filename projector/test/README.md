Test Servers - Remote control for projectors

- Initialy, only network-connected projectors can be controlled.

Roadmap:

  - Connection handler
  - PJLink module
  - Other projectors as needed
  - Possibly add other connection methods (i.e., serial port)

Modularization:
    Projector emulators are modularized to make it easer to test different
    types of projector interfaces.

    Projector personalities are modules in the servers/ directory

Projector personalities are threaded, so ensure your new server modules are
thread safe.

server-net.py
    Network connection handler. Once a new server module is added, update the
    argparse section with the proper code to load your new module.
    Default module will be the PJLink protocol server.

    Example:

    log.debug("Setting projector type")
    if args.type.upper() == 'PJLINK':
        log.info("Loading PJLink projector settings")
        from servers.PJLink import *
        projector = Projector()
        projector.start()

    elif args.type.upper() == 'YOURNEWSERVER':
        log.info("Loading YourNewServer projector settings")
        from servers.YourNewServer import *
        # Don't forget to include any (opts), if needed, here
        projector = Projector(opts)
        projector.start()
