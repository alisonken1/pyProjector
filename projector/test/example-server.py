# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=80 tabstop=4 softtabstop=4

 """example-server.py: Example skeleton module for creating a new server"""


__name__ = "example-server"
__version__ = "0.0.1"
_v = __version__.split(".")
__version_hex__ = int(_v[0]) << 24 | \
                  int(_v[1]) << 16 | \
                  int(_v[2]) << 8
__all__ = ['BUFFER', 'HEADER', 'PORT', 'SUFFIX', 'TIMEOUT_MAX', 'Projector']

import logging as log
import threading
import time
import random
from time import sleep
from hashlib import md5
from socket import timeout
log.info("example-server %s module loading" % __version__)

log.info("example-server: Setting required variables")
PORT = 4352
BUFFER = 140
TIMEOUT_MAX = 30.0

log.info("example: Setting local constants")
CR = chr(0x0D) # \r
LF = chr(0x0A) # \n
SUFFIX = CR # Suffix for your emulated server - default PJLink
PREFIX = '%' # Prefix character(s) for emulated server - default PJLink
CLASS = '1'  # Command class if needed - default PJLink
HEADER = PREFIX + CLASS # Modify for your server

log.info("example-server: Creating Projector() class")
class Projector(threading.Thread):
    def __init__(self,
                 group=None,
                 target=None,
                 name='example-server',
                 user=None,
                 password = None
                 args=[],
                 kwargs={}):
        super(Projector, self).__init__(group, target, name, args, kwargs)
        self.name = name
        # Fill in self.info with proper keys for information based on
        # the emulated projector being coded for
        self.info = { }
        self.running = False
        self.user = user
        self.passwd = password
        self.hashes = {} # Dictionary to hold multiple login hashes
        self.hashstring = None

    def stop(self):
        """stop() Clears running flag so thread can safely stop"""
        log.debug('example-server: stop() called')
        self.running = False
        return

    def run(self):
        """run() Runs while thread is alive"""
        log.debug('example-server: run() called')
        self.running = True
        while self.running:
            # Add code to handle runtime settings
            sleep(10)
        return

    def client_connect(self, client, addr):
        """client_connect() Handle connection initialization with client"""
        log.debug("example-server: client_connect(addr='%s')" % repr(addr))
        # This check should be for user and/or password options
        if self.passwd is None:
            """Add code for non-authenticated connection request"""
            pass
        else:
            """Add code for authenticated connection request"""
            pass
        return

    def client_disconnect(self, addr):
        """client_disconnect() Cleans up from client disconnect"""
        log.debug("example-server: client_disconnect(addr=%s)" % repr(addr))
        _ip, _port = addr
        return

    def handle_request(self, opts):
        log.debug("example-server: handle_request(opts='%s')" % opts)
        return
