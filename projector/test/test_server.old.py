#!/usr/bin/env python
"""test_server.py

Test server for checking PJLink controllers.
Warning: Not multi-connection safe - use only one instance at a time
"""

import thread
import threading
import random
import time
import sys
from time import sleep
from socket import *
from hashlib import md5

# Static defines
BUFFER = 136 # Maximum packet size including \n
HOST = '127.0.0.1'
SERVICE = { 'PORT': 4352,
            'NAME': 'pjlink'
          }
PJLINK_PREFIX = "%"
PJLINK = { "CLASS": '1',
           "COMMANDS": { '1': ['POWR',
                               'INPT',
                               'AVMT',
                               'ERST',
                               'LAMP',
                               'INST',
                               'NAME',
                               'INF1',
                               'INF2',
                               'INFO',
                               'CLSS'
                              ]
                       }
         }

PJLINK_ERR = { 'OK': "Success",
               'ERR1': "Undefined command",
               'ERR2': "Out of parameter",
               'ERR3': "Unavailable time",
               'ERR4': "Projector/Display error",
               0: 'OK',
               1: 'ERR1',
               2: 'ERR2',
               3: 'ERR3',
               4: 'ERR4',
               10: "Empty packet",
               11: "Invalid prefix",
               12: "Invalid class",
               13: "Invalid packet"
               }

TIMEOUT_MAX = 30.0 # Number of idle seconds before forced close
CR = chr(0x0D) # \r
LF = chr(0x0A) # \n
CRLF = CR + LF

# Authentication purposes
PASSWD = None

# Dictionary of commands mapped to functions
PJLINK_CMD = {}

projector = None # used for class Projector
class Projector(threading.Thread):
    """Threaded projector class

    Acts as a single projector that will update itself outside of the socket
    thread.
    """
    def __init__(self, group=None,
                       target=None,
                       name="ProjectorThread",
                       args=[], kwargs={}):
        super(Projector, self).__init__(group, target, name, args, kwargs)
        self.name = name
        self.info = { 'NAME': "DefaultProjector", # Can be changed"
                      'INF1': "OpenLP",
                      'INF2': "PythonProjector",
                      'INFO': "Model PS01 Serial P274001"
                    }
        self.model = 'PS01' # Python single lamp model 1
        self.other = "Serial number P274001"
        self.lockedPower = threading.Lock() # Power changes
        self.lockedVideo = threading.Lock() # Video changes
        self.lockedLamp = threading.Lock()  # Lamp changes
        self.PROJECTOR_ERROR = None # ERR3 or ERR4
        # See self.powerstat for status,fan values
        self.lamp = { 0: {"status": '0',
                          "time": random.randint(0, 30000), # time in minutes
                          "clock": 0, # Clock time when lamp was turned on
                          "fan": '0'
                          }
                    }
        self.time = 0
        for i in self.lamp:
            # Random failure of lamp on startup
            self.lamp[i]['status'] = 4 if int(random.random()) == 1 \
                                        else 0
            if self.lamp[i]['status'] > 3: self.PROJECTOR_ERROR = PJLINK_ERR[4]
            self.time = self.lamp[i]['time'] \
                if self.time < self.lamp[i]['time'] \
                else self.time
        self.time = self.time + 60 # 1 hour longer than longest lamp time
        # self.powerstat
        #   0 = off
        #   1 = startup
        #   2 = standby
        #   3 = warmup - check self.lamp
        #   4 = on
        #   5 = cooldown check self.lamp
        #   6 = warning
        #   7 = error
        self.powerstat = 0
        self.powerup = 0 # Timer startup/lamp on/lamp off
        self.audio_mute = False # False=aduio out True=audio mute
        self.video_mute = False # False=open True=closed
        self.video = '11'
        self.video_select = { 1: 4, # 1=vga, 2=rgb1, 3=rgb2, 4=dvi-a
                              2: 3, # 1=composite, 2=svideo, 3=SCART
                              3: 3, # 1=dvi-i, 2=dvi-d, 3=hdmi
                              4: 0, # 1=usb input, 2=internal storage input
                              5: 0,  # 1=network input
                              '11': "Computer RGB",
                              '12': "Analog RGB",
                              '13': "SCART1",
                              '14': "DVI-A",
                              '21': "Composite Video",
                              '22': "S-Video",
                              '23': "SCART2",
                              '31': "DVI-I",
                              '32': "DVI-D",
                              '33': "HDMI"
                            }
        self.temp = 0 # 0=ok, 1=warning 2=error
        self._filter = 0 # 0=ok, 2=warning, 3=error

    def stop(self):
        self._running = False

    def run(self):
        """Start projector and maintain projector status"""
        self._running = True
        print "%s Projector class run called" % self.name
        self.PROJECTOR_ERROR = PJLINK_ERR[3]
        print "%s Projector status 1: startup" % self.name
        self._powerchange('1')
        while self._running:
            sleep(2) # 2 second sleep interval between updates
            # Check for powerup status
            if self.powerup is not None and self.powerup < time.time():
                # Time to change status
                print "Projector.run(): Changing powerup state"
                if self.powerstat == '5':
                    self._powerchange(2)
                else:
                    self._powerchange(str(int(self.powerstat) + 1))

    def handle_request(self, cmd, opt):
        print "Projector.handle_request(cmd='%s' opt='%s'" % (cmd, opt)
        if self.PROJECTOR_ERROR is not None:
            return self.PROJECTOR_ERROR
        if not cmd in PJLINK["COMMANDS"]['1']:
            return PJLINK_ERR[1]

        elif cmd == "POWR":
            return self.power(opt)

    def _lampchange(self, lamp, opt):
        """Projector._lampchange(): Change lamp and fan state"""
        # Called by self._powerchange
        # See 234567 of self.powerstat
        print "Projector._lampchange(opt='%s')" % opt
        if lamp['status'] == '7' or lamp['fan'] > '7':
            # Error condition
            return PJLINK_ERR[4]

        with self.lockedLamp:
            if opt == '2':
                # Lamp off
                if lamp['clock'] is not None:
                    _c = int(time.time() - lamp['clock']) / 60
                    lamp['time'] = lamp['time'] + _c
                    lamp['clock'] = None
                    lamp['fan'] = 2

            elif opt == '3':
                # Lamp warmup to on
                lamp['clock'] = time.time()
                lamp['fan'] = 4
                
            lamp['status'] = opt

        return PJLINK_ERR[0]

    def _powerchange(self, opt):
        """Projector._poewrchange(): Change power state"""
        print "Projector._powerchange(opt='%s')" % opt
        # Called by 
        if not opt in '012345': return 
        with self.lockedPower:
            if opt == '1':
                self.PROJECTOR_ERROR = PJLINK_ERR[3] \
                    if self.PROJECTOR_ERROR != PJLINK_ERR[4] \
                    else self.PROJECTOR_ERROR
            if opt in '135':
                self.powerup = time.time() + 30.0 \
                    if opt == 1 \
                    else time.time() + 60
            elif opt in '024':
                self.powerup = None
                self.PROJECTOR_ERROR = None \
                    if self.PROJECTOR_ERROR != PJLINK_ERR[3] \
                        or self.PROJECTOR_ERROR != PJLINK_ERR[4] \
                    else self.PROJECTOR_ERROR
            self.powerstat = opt

        r = PJLINK_ERR[0]
        for i in self.lamp:
            j = self._lampchange(self.lamp[i], opt)
            r = j if r != PJLINK_ERR[4] else r
        return r

    def power(self, opt=None):
        """Set/get power status - Called from self.handle_request()"""
        print "Projector.power(opt='%s')" % opt
        # Initial error checks
        try:
            if opt not in '01?':
               return PJLINK_ERR[2]
        except TypeError:
            return PJLINK_ERR[2]
        # Status query
        if self.PROJECTOR_ERROR is not None:
            return self.PROJECTOR_ERROR
        elif opt == '?':
            print 'Projector.power() self.powerstat = %s' % self.powerstat
            if self.powerstat == 2: return '0'
            elif self.powerstat == 4: return '1'
            elif self.powerstat == 5: return '2'
            elif self.powerstat == 3: return '3'
            elif self.powerstat >= 6: return PJLINK_ERR[4]

        # Time to change power
        if self.powerstat == 1 and self.powerup is not None: return PJLINK_ERR[3]
        elif self.powerstat in '35' and self.powerup is not None: return PJLINK_ERR[0]

        if opt == '1':
            if self.powerstat == 4:
                # Already on
                return PJLINK_ERR[0]
            elif self.powerstat == 5: 
                # Power up during cooldown
                return PJLINK_ERR[3]
            elif self.powerstat == 2:
                # Set to warmup
                return self._powerchange('3')
        elif opt == '0':
            if self.powerstat == 2: 
                # Aldready in standby
                return PJLINK_ERR[0]
            elif self.powerstat == 3:
                # Power off during warmup
                return PJLINK_ERR[3]
            elif self.powerstat == 4:
                # Set to cooldown
                return self._powerchange('5')

def logme(data):
    """Print socket logging info to console"""
    print repr(addr) + " " + data

def getRandom():
    """Return an 8-char random string"""
    return str(hex(random.randint(268435456, 4294967295L)))[2:-1]

def send(data):
    """Send data to client socket"""
    clientsock.send(data + CRLF)

def handler(clientsock, addr, seed=None, hashed=None):
    """Socket handler"""
    print "New thread addr='%s' seed='%s' hashed='%s" % \
        (repr(addr), seed, hashed)
    clientsock.settimeout(TIMEOUT_MAX)
    handler_running = True
    while handler_running:
        RESPONSE = None
        ERR = 0 # > 10 non-standard error for internal use
        try:
            d = clientsock.recv(BUFFER + 1) # Allow for braindamaged \r
            data = d.strip((CR))
            if not data: break
            if '\xff\xf4\xff\xfd\x06' in data:
                # ^C pressed - close connection
                print repr(addr) + " Received ^C"
                break
            logme("Received: '%s' " % repr(data))
        except timeout, e:
            break
        # OK - got data, time to verify if proper format
        # If invalid prefix - ignore
        if len(data) <= 1:
            ERR = 10
        elif len(data) <= 8 or len(data) > BUFFER:
            # Invalid packet
            logme("Invalid packet length: %s data: '%s' " % \
                (len(data), repr(data)))
        elif data[0] != PJLINK_PREFIX:
            # Invalid start character - ignore
            logme("Invalid prefix: %s" % data[0])
            ERR = 11 # Invalid prefix
        elif not data[1] in PJLINK['CLASS']:
            # invalid class = send ERR1
            logme("Invalid class: %s " % data[1])
            ERR = 12 # Invalid class
        elif data[6] != ' ':
            ERR = 1
        else:
            # Basic packet checks OK - process rest of command
            pass

        # End of checks - time to send back
        if ERR == 0:
            # packet checks OK
            pass
        elif ERR == 10 and RESPONSE is None:
            # Invalid packet - ignore
            logme("Blank packet received")
            send('')
            continue
        elif ERR > 10:
            send("%s=%s" % (data[:6].strip(), PJLINK_ERR[1]))
            continue
        elif ERR >= 1 and ERR in PJLINK_ERR:
            send("%s=%s" % (data[:6].strip(), PJLINK_ERR[ERR]))
            continue
        else:
            logme("Unknown error %s" % ERR)
            continue

        cmd = data[2:6].upper().strip()
        cls = data[1]
        response = projector.handle_request(cmd, data[7:].strip())
        data = data[:2].strip() + cmd + "=" +response + CRLF
        logme("Sending %s" % data)
        send(data)
    # Done with the socket - time to cleanup
    clientsock.close()
    print "Connection closed addr=" + repr(addr)
    if not handler_running:
        print "handler terminated"

if __name__ == '__main__':
    # Start the projector
    if projector is None:
        projector = Projector()
        projector.start()
    # Start the socket service
    ADDR = (HOST, SERVICE['PORT'])
    serversock = socket(AF_INET, SOCK_STREAM)
    serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    serversock.bind(ADDR)
    serversock.listen(1)
    main_running = True
    while main_running:
        try:
            print "Waiting for connection ... listening on port" + \
                str(SERVICE["PORT"])
            clientsock, addr = serversock.accept()
            print "Connected from:" + repr(addr)
            if PASSWD is None:
                mainthread = thread.start_new_thread(handler, (clientsock, addr))
            else:
                rand = getRandom()
                hashed = md5()
                hashed.update(rand)
                hashed.update(PASSWD)
                thread.start_new_thread(handler,
                                        (clientsock,
                                            addr,
                                            rand,
                                            hashed.hexdigest())
                                        )
        except KeyboardInterrupt:
            print "Keyboard interrupt - stopping"
            projector.stop()
            handler_running = False
            main_running = False
